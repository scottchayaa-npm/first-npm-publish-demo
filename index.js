module.exports.add = (x, y) => {
  return x + y;
}

module.exports.sum = (items) => {
  let result = 0;
  for (let i of items) {
    result += i;
  }
  return result;
}