
# First-npm-publish-demo

[![pipeline status](https://gitlab.com/scottchayaa-npm/first-npm-publish-demo/badges/master/pipeline.svg)](https://gitlab.com/scottchayaa-npm/first-npm-publish-demo/commits/master)
[![coverage report](https://gitlab.com/scottchayaa-npm/first-npm-publish-demo/badges/master/coverage.svg)](https://gitlab.com/scottchayaa-npm/first-npm-publish-demo/commits/master)

For tutorial(blog) : [link](https://blog.scottchayaa.com/post/2019/07/04/npm/)  
This my demo npm module.  

# How to use

```sh
yarn add first-npm-publish-demo
```

```js
const demo = require('first-npm-publish-demo');
console.log(demo.add(2, 3)); // 5
console.log(demo.sum([2, 3, 4])); // 9
```
