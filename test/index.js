const tap = require('tap');
const app = require("../index");


tap.test('test add()', function (t) {
    t.plan(1);
    t.equal(app.add(1, 2), 3);
});

tap.test('test sum()', function (t) {
    t.plan(1);
    t.equal(app.sum([1, 2, 3]), 6);
});
